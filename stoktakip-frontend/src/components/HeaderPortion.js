import React from 'react'

function HeaderPortion() {
    return (
        <div style={{backgroundColor:"#001626", width:"100%", color:"#E9F4FF"}}>
            <h1>KolayStok</h1>
        </div>
    )
}

export default HeaderPortion