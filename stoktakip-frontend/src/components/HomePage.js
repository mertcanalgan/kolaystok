import {Layout, Space} from "antd";
import {Content, Header} from "antd/es/layout/layout";
import Sider from "antd/es/layout/Sider";
import SideBar from "./SideBar";
import HeaderPortion from "./HeaderPortion";
import List from "./List";
import '../style/App.css';
import {useEffect, useState} from "react";
import EnvanterList from "./EnvanterList";
import PersonelList from "./PersonelList";

function HomePage() {
    const headerStyle = {
        textAlign: 'center',
        color: '#001d3a',
        backgroundColor: '#001626',
        borderLeft : "2px solid black",
        borderRight:"2px solid black"
    };
    const contentStyle = {
        textAlign: 'center',
        width:"75%",
        color: '#FFFFFF',
        backgroundColor: '#FFFFFF',
        borderTop : "2px solid black",
        borderBottom : "2px solid black",
        borderColor:'#001528',
    };
    const siderStyle = {
        textAlign: 'center',
        width:"25%",
        height: 750,
        backgroundColor: '#001626',
        border : "2px solid black",
        borderColor:'#001626',

    };
    const kullaniciAdi = localStorage.getItem("kullaniciAdi");
    const sifre = localStorage.getItem("sifre");
    const [content,setContent] = useState(null);
    useEffect(() => {
        if (kullaniciAdi && sifre) {
            setContent(<PersonelList></PersonelList>);
        }
    }, [kullaniciAdi, sifre]);
    const handleClickPersonel = () =>{
        const kullaniciAdi = localStorage.getItem('kullaniciAdi');
        const sifre = localStorage.getItem('sifre');
        const expirationTime = localStorage.getItem('expirationTime');

        if (kullaniciAdi && sifre && expirationTime) {
            setContent(<PersonelList></PersonelList>)
        }else {
            setContent(null)
        }
    }
    const handleClickDemirbas = () => {
        const kullaniciAdi = localStorage.getItem('kullaniciAdi');
        const sifre = localStorage.getItem('sifre');
        const expirationTime = localStorage.getItem('expirationTime');

        if (kullaniciAdi && sifre && expirationTime) {
            setContent(<EnvanterList></EnvanterList>)
        } else {
            setContent(null)
        }

    }
    return (
        <div className="App" style={{"height":"100%","borderRadius":5 }}>
            <Space
                direction="vertical"
                style={{
                    width: '100%',
                    height: '100%'
                }}
            >

                <Layout>
                    <Header style={headerStyle}> <HeaderPortion></HeaderPortion> </Header>
                    <Layout>
                        <Sider style={siderStyle}><SideBar handleClickPersonel={handleClickPersonel} handleClickDemirbas={handleClickDemirbas}></SideBar> </Sider>
                        <Content style={contentStyle}>
                            {content && <List content={content}></List>}
                        </Content>
                    </Layout>
                </Layout>
            </Space>

        </div>
    );
}

export default HomePage;