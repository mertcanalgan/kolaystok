import React from 'react'
import {Button} from "antd";


function SideBar(props) {
    const logOut = () =>{
        localStorage.removeItem("kullaniciAdi");
        localStorage.removeItem("sifre");
        localStorage.removeItem("expirationTime");
    }

    return (
        <div>
            <Button type={"primary"} size={"large"} onClick={props.handleClickPersonel}  block={true} style={{"height":"10em","width":"100%", borderRadius:5,borderLeft:"1px solid black",borderTop:"0px", backgroundColor:"#EB5800"}}>Personel Listesi</Button>
            <Button type={"primary"} onClick={props.handleClickDemirbas}  style={{borderRadius:5,"width":"100%", "height":"10em",border:"1px solid black", borderRight:"0px",backgroundColor:"#EB5800"}} size={"large"}>Demirbaş Listesi</Button>
            <Button href={"/"} onClick={logOut} type={"primary"} style={{position: 'absolute',
                    bottom:0,
                    left:0,
                borderRadius:5,
                    borderTop:"1px solid black",
                    textAlign:"center",
                    padding:"1.5em", "height":"5em",backgroundColor:"#D1000E"}} danger block={true}>Çıkış Yap</Button>
        </div>

    )
}

export default SideBar