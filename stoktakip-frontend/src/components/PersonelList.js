import React, {useEffect, useState} from 'react'
import {Button, Input, Modal, Table} from "antd";
import axios from "axios";
import {DeleteOutlined, DownloadOutlined, EditOutlined, LoadingOutlined} from "@ant-design/icons";
import PDFFile from "../pdf/PDFFile";
import {PDFDownloadLink} from "@react-pdf/renderer";
const ExpandedRow = ({ record }) => {
    const [personelZimmet, setPersonelZimmet] = useState([]);
    const [isDeleteDemirbasFromPersonel,setIsDeleteDemirbasFromPersonel] = useState(false);
    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`http://localhost:8080/api/demirbas/${record.id}`);
            setPersonelZimmet(result.data);
        };
        fetchData();
    }, [record.id,isDeleteDemirbasFromPersonel]);
    const handleDeleteDemirbasFromPersonel = (record) => {
        Modal.confirm({
            title:"Bu personelden bu demirbaşı kaldırmak istiyor musunuz?",
            okText : "Evet",
            cancelText: "Vaz geç",
            okType:"danger",
            onOk: () =>{
                axios({
                    method: "put",
                    url: `http://localhost:8080/api/demirbas`,
                    data: {
                        ...record,
                        personel: null,
                    },
                })
                    .then(() => {
                        setIsDeleteDemirbasFromPersonel(true);
                        return personelZimmet;
                    })
                    .catch((error) => {
                        console.error("Error deleting resource:", error);
                    });
                setPersonelZimmet((pre)=>{
                    return pre.filter((personelZimmet)=>personelZimmet.id !== record.id)
                });
            }
        })

    };


    const detayColumns = [
        {

            title: 'Tip',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title:"Seri No",
            dataIndex:"seriNo",
            key:"seriNo"
        },
        {
            title:"Cihaz Markası",
            dataIndex:"marka",
            key:"marka"
        },
        {
            title:"Cihaz Modeli",
            dataIndex:"model",
            key:"model"
        },
        {
            title:"Lokasyon",
            dataIndex: "lokasyon",
            key: "lokasyon"
        },
        {
            title: "Mülkiyet",
            dataIndex: "mulkiyet",
            key: "mulkiyet"
        },
        {
            title: "İşlemler",
            render: (record)=>{
                return(
                    <>
                        <DeleteOutlined
                            title={"Demirbaşı Personelden Çıkar"}
                            style={{ marginLeft: 12, color: "#ff4d4f" }}
                            onClick={()=>handleDeleteDemirbasFromPersonel(record)}
                        ></DeleteOutlined>
                    </>
                )
            }
        },
    ]


    return (
        <>
            <Table dataSource={personelZimmet} columns={detayColumns} pagination={false} />
        </>
    );
};


function PersonelList() {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isEditing,setIsEditing] = useState(false);
    const [editingPersonel, setEditingPersonel] = useState(true)
    const [personel,setPersonel] = useState([]);
    const [expandTable,setExpandTable] = useState([]);
    const [addingPersonel,setAddingPersonel] = useState({});
    const [searchText,setSearchText] = useState('');
    useEffect(()=>{
        axios
            .get("http://localhost:8080/api/personel")
            .then((res)=>setPersonel(res.data))
            .catch((err)=>console.log(err));
    },[]);


    useEffect(() => {
        if(personel){
            const addedKeyList = []
            personel.forEach((x,i)=> {
                x.key=i
                addedKeyList.push(x)
                }
            )
            setExpandTable(addedKeyList)
        }
    }, [personel]);


    const filterData = (personel,searchText) =>{
        return personel.filter(item => item.isim.toLowerCase().includes(searchText.toLowerCase()));
    };
    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        setIsModalOpen(false);
        setPersonel((pre) => {
            return [...pre, addingPersonel];
        });
        axios({
            method: "post",
            url: "http://localhost:8080/api/personel",
            data: {
                isim: addingPersonel.isim,
                soyIsim: addingPersonel.soyIsim,
                lokasyon: addingPersonel.lokasyon,
                birim: addingPersonel.birim,
            },
        });
        setAddingPersonel("");
    };
    const handleCancel = () => {
        setIsModalOpen(false);
        setAddingPersonel("");
    };
    const deletePersonel = (record) =>{
        Modal.confirm({
            title: "Emin misiniz?",
            okText: "Evet",
            okType: "danger",
            onOk: ()=>{
                axios.delete(`http://localhost:8080/api/personel/${record.id}`)
                    .then((response) => {
                        console.log("Resource deleted:", response.data);
                    })
                    .catch((error) => {
                        console.error("Error deleting resource:", error);
                    });
                setPersonel((pre) => {
                    return pre.filter((personel) => personel.id !== record.id);
                });
            }
        })
    }

    const editPersonel = (record)=>{
        setIsEditing(true);
        setEditingPersonel({...record});
    };
    const resetEditing = ()=>{
        setIsEditing(false);
        setEditingPersonel(null);
    };
    const degisenPersonel = () => {
        axios({
            method: "put",
            url: "http://localhost:8080/api/personel",
            data: {
                id:editingPersonel.id,
                isim: editingPersonel.isim,
                soyIsim: editingPersonel.soyIsim,
                lokasyon: editingPersonel.lokasyon,
                birim: editingPersonel.birim,
            },
        });
        return editingPersonel;
    };

    const columns = [
        {
            title: 'Id',
            dataIndex: 'id',
            sorter: (a, b) => a.id - b.id,
            defaultSortOrder: 'ascend',
            sortOrder: 'ascend',
            key: 'id',
        },
        {
            title: 'İsim',
            dataIndex: 'isim',
            key: 'isim',
        },
        {
            title: 'Soyisim',
            dataIndex: 'soyIsim',
            key: 'soyIsim',
        },
        {
            title: 'Birim',
            dataIndex: 'birim',
            key: 'birim'
        },
        {
            title: 'Lokasyon',
            dataIndex: 'lokasyon',
            key: 'lokasyon'
        },
        {
            title: "İşlemler",
            render: (record)=>{
                return(
                    <>
                        <EditOutlined
                            title={"Personeli Düzenle"}
                            style={{color:"#1777ff"}}
                            onClick={() => {
                                editPersonel(record);
                            }}
                        ></EditOutlined>
                        <DeleteOutlined
                            title={"Personeli Sil"}
                            onClick={() => {
                                deletePersonel(record);}}
                            style={{ marginLeft: 12,color:"#ff4d4f" }}
                        ></DeleteOutlined>
                        <PDFDownloadLink document={<PDFFile merhaba={record}/>} fileName={`${record.isim}-${record.soyIsim}`}>
                            {({loading}) =>(loading ? <LoadingOutlined style={{color:"black"}} /> : <DownloadOutlined title={"Zimmetli Eşyaları İndir"} style={{ marginLeft: 12, color:"black" }}></DownloadOutlined> )}
                        </PDFDownloadLink>
                    </>

                )
            }
        },
        Table.EXPAND_COLUMN,
        {
            title: "Detay",
            dataIndex: "detay",
            key:"detay"
        },
    ];
    return (
        <>
            <h1 style={{textAlign:"center",color:"black"}}>Personel Listesi</h1>
            <Input.Search placeholder="Personel Arama" allowClear onChange={e => setSearchText(e.target.value)} style={{marginBottom:16, width:"50%"}}></Input.Search>
            <Table pagination={{pageSize:6}} style={{height:"100%"}} expandedRowRender={(record) => <ExpandedRow record={record} />}
                   columns={columns} dataSource={filterData(expandTable,searchText)}  size="small"/>
            <Button type="primary" style={{marginTop:"10px"}} onClick={showModal}>
                Personel Ekle
            </Button>
                <Modal title="Personel Ekle" style={{textAlign:"center"}} open={isModalOpen} okText={"Kaydet"} cancelText={"İptal"} onOk={handleOk} onCancel={handleCancel}>
                <Input placeholder="İsim" value={addingPersonel.isim} id="isim" name="isim" onChange={(e)=>{setAddingPersonel((pre)=>{return {...pre,isim:e.target.value};});}}></Input>
                <Input placeholder="Soyisim" style={{marginTop:10}} value={addingPersonel.soyIsim} id="soyIsim" name="soyIsim" onChange={(e)=>{setAddingPersonel((pre)=>{return {...pre,soyIsim:e.target.value};});}}></Input>
                <Input placeholder="Birim" style={{marginTop:10}} value={addingPersonel.birim} id="birim" name="birim" onChange={(e)=>{setAddingPersonel((pre)=>{return {...pre,birim:e.target.value};});}}></Input>
                <Input placeholder="Lokasyon" style={{marginTop:10}} value={addingPersonel.lokasyon} id="lokasyon" name="lokasyon" onChange={(e)=>{setAddingPersonel((pre)=>{return {...pre,lokasyon:e.target.value};});}}></Input>
            </Modal>
            <Modal
                title="Peronel düzenle"
                open={isEditing}
                okText="Kaydet"
                onCancel={() => {
                    resetEditing();
                }}
                onOk={() =>{
                    setExpandTable((pre)=>{
                    return pre.map((personel)=> {
                        if (personel.id === editingPersonel.id) {
                            return degisenPersonel();
                        } else {
                            return personel;
                        }
                    });
                    });
                    resetEditing();
                    }}
            >
                <Input
                    value={editingPersonel?.isim}
                    onChange={(e) => {
                        setEditingPersonel((pre) => {
                            return { ...pre, isim: e.target.value };
                        });
                    }}
                ></Input>
                <Input
                    style={{ marginTop: 10 }}
                    value={editingPersonel?.soyIsim}
                    onChange={(e) => {
                        setEditingPersonel((pre) => {
                            return { ...pre, soyIsim: e.target.value };
                        });
                    }}
                ></Input>
                <Input
                    style={{ marginTop: 10 }}
                    value={editingPersonel?.birim}
                    onChange={(e) => {
                        setEditingPersonel((pre) => {
                            return { ...pre, birim: e.target.value };
                        });
                    }}
                ></Input>
                <Input
                    style={{ marginTop: 10 }}
                    value={editingPersonel?.lokasyon}
                    onChange={(e) => {
                        setEditingPersonel((pre) => {
                            return { ...pre, lokasyon: e.target.value };
                        });
                    }}
                ></Input>
            </Modal>
            </>



    )
}

export default PersonelList;