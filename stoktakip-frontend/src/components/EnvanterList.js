import React, {useEffect, useState} from 'react'
import {Button, Input, Modal, Select, Table} from "antd";
import axios from "axios";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";
import '../style/Table.css';
import {PDFDownloadLink} from "@react-pdf/renderer";
import PDFDemirbasTable from "../pdf/PDFDemirbasTable";


function EnvanterList() {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [,setExpandTable] = useState([]);
    const [demirbas,setDemirbas] = useState([]);
    const [personel,setPersonel] = useState([]);
    const [addingDemirbas,setAddingDemirbas]=useState({});
    const [searchText,setSearchText] = useState('');
    const [isEditing,setIsEditing] = useState(false);
    const [editingDemirbas, setEditingDemirbas] = useState(true);
    const [createdDemirbas,setCreatedDemirbas] = useState({});
    const [selectedOption, setSelectedOption] = useState(null);
    const {Option} = Select;


    useEffect(()=>{
        axios.get("http://localhost:8080/api/personel")
            .then((response)=>setPersonel([...response.data,{id:0,isim:"Personelsiz"}]))
            .catch((err)=>console.log(err));
    },[]);
    useEffect(()=>{
        axios.get("http://localhost:8080/api/demirbas")
            .then((response)=>setDemirbas(response.data))
            .catch((err)=>console.log(err));
    },[]);
    useEffect(()=>{
        if(createdDemirbas.id){
            axios.get("http://localhost:8080/api/demirbas")
                .then((response) => setDemirbas(response.data))
                .catch((err) => console.log(err));
            setCreatedDemirbas({});
        }
    },[createdDemirbas]);
    useEffect(()=>{
            axios.get("http://localhost:8080/api/demirbas")
                .then((response) => setDemirbas(response.data))
                .catch((err) => console.log(err));
    },[editingDemirbas]);
    useEffect(() => {
        if(demirbas){
            const addedKeyList = []
            demirbas.forEach((x,i)=> {
                    x.key=i
                    addedKeyList.push(x)
                }
            )
            setExpandTable(demirbas)
        }
    }, [demirbas]);

    const expandedRowRender = (record) => {
            return <Table columns={detayColumns} dataSource={[record]} pagination={false} />;
    }
    const filterData = (demirbas,searchText) =>{
        return demirbas.filter(item => item.type.toLowerCase().includes(searchText.toLowerCase()));
    };
    const deleteDemirbas = (record) =>{
        Modal.confirm({
            title:"Emin misiniz?",
            okText : "Evet",
            okType:"danger",
            onOk: ()=>{
                axios.delete(`http://localhost:8080/api/demirbas/${record.id}`)
                    .then((response) => {
                        console.log("Resource deleted:", response.data);
                    })
                    .catch((error) => {
                        console.error("Error deleting resource:", error);
                    });
                setDemirbas((pre)=>{
                    return pre.filter((demirbas) => demirbas.id !== record.id)
                });
            }
        })
    }

    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        if (addingDemirbas.type != null){
            setIsModalOpen(false);
            setDemirbas((pre)=>{
                return [...pre,addingDemirbas];
            });
            axios({
                method : "post",
                url: "http://localhost:8080/api/demirbas",
                data:{
                    seriNo:addingDemirbas.seriNo,
                    type : addingDemirbas.type,
                    marka : addingDemirbas.marka,
                    personelIsim : addingDemirbas.personelIsim,
                    personelSoyIsim : addingDemirbas.personelSoyIsim,
                    lokasyon: addingDemirbas.lokasyon,
                    mulkiyet: addingDemirbas.mulkiyet,
                    model: addingDemirbas.model,
                    cpu : addingDemirbas.cpu,
                    gpu : addingDemirbas.gpu,
                    ram : addingDemirbas.ram,
                    hafiza : addingDemirbas.hafiza,
                    ekranBoyutu: addingDemirbas.ekranBoyutu,
                    tahsisTarihi: addingDemirbas.tahsisTarihi,
                    ekranCozunurlugu : addingDemirbas.ekranCozunurlugu,
                    alimTarihi : addingDemirbas.alimTarihi,
                    alinanFirma : addingDemirbas.alinanFirma,
                    alinanFirmaIrtibat : addingDemirbas.alinanFirmaIrtibat,
                    personel : addingDemirbas.personelId === 0 ? null : personel.find(personel =>addingDemirbas.personelId === personel.id),
                },
            }).then((res)=>setCreatedDemirbas(res.data));
            setSelectedOption(null);
            setAddingDemirbas({
                seriNo: "",
                type: null,
                marka: "",
                personelIsim: "",
                personelSoyIsim: "",
                lokasyon: "",
                mulkiyet: "",
                model: "",
                cpu: "",
                gpu: "",
                ram: "",
                hafiza: "",
                ekranBoyutu: "",
                tahsisTarihi: null,
                ekranCozunurlugu: "",
                alimTarihi: null,
                alinanFirma: "",
                alinanFirmaIrtibat: "",
                personelId: null
            });
        }else{
            alert("Demirbaş tipi girilmeli");
        }
    };
    const handleCancel = () => {
        setIsModalOpen(false);
        setAddingDemirbas("");
        setSelectedOption(null);
    };
    const getRowClassName = (record) => {
        if (!record?.personel?.id ) {
            return 'missing-data-row';
        }
        return '';
    };
    const editDemirbas = (record)=>{
        setIsEditing(true);
        setEditingDemirbas({...record});
    };
    const resetEditing = ()=>{
        setIsEditing(false);
        setEditingDemirbas(null);
    };



    const handleChangeOk = () => {
        if (editingDemirbas.type != null){
            setIsModalOpen(false);
            setDemirbas((pre)=>{
                return [...pre,editingDemirbas];
            });
            axios({
                method: "put",
                url: "http://localhost:8080/api/demirbas",
                data: {
                    id:editingDemirbas.id,
                    type: editingDemirbas.type,
                    seriNo: editingDemirbas.seriNo,
                    lokasyon: editingDemirbas.lokasyon,
                    mulkiyet: editingDemirbas.mulkiyet,
                    marka: editingDemirbas.marka,
                    model: editingDemirbas.model,
                    cpu: editingDemirbas.cpu,
                    gpu: editingDemirbas.gpu,
                    ram: editingDemirbas.ram,
                    hafiza: editingDemirbas.hafiza,
                    ekranBoyutu: editingDemirbas.ekranBoyutu,
                    ekranCozunurlugu: editingDemirbas.ekranCozunurlugu,
                    tahsisTarihi: editingDemirbas.tahsisTarihi,
                    alimTarihi: editingDemirbas.alimTarihi,
                    alinanFirma: editingDemirbas.alinanFirma,
                    alinanFirmaIrtibat: editingDemirbas.alinanFirmaIrtibat,
                    personel: editingDemirbas.personel?.id === 0 ? null : personel.find(personel => editingDemirbas.personel?.id === personel.id),
                },
            }).then((res)=>setEditingDemirbas(res.data))
        }else{
            alert("Demirbaş tipi girilmeli");
        }
    };

    const detayColumns = [
        {
            title:"İşlemci",
            dataIndex:"cpu",
            key:"cpu"
        },
        {
            title:"Ram",
            dataIndex:"ram",
            key:"ram"
        },
        {
            title:"Depolama",
            dataIndex:"hafiza",
            key:"hafiza"
        },
        {
            title:"Ekran Kartı",
            dataIndex:"gpu",
            key:"gpu"
        },
        {
            title:"Ekran Boyutu",
            dataIndex:"ekranBoyutu",
            key:"ekranBoyutu"
        },
        {
            title:"Ekran Çözünürlüğü",
            dataIndex:"ekranCozunurlugu",
            key:"ekranCozunurlugu"
        },
        {
            title:"Tahsis Tarihi",
            dataIndex:"tahsisTarihi",
            key:"tahsisTarihi"
        },
        {
            title:"Alım Tarihi",
            dataIndex:"alimTarihi",
            key:"alimTarihi"
        },
        {
            title:"Alınan Firma",
            dataIndex:"alinanFirma",
            key:"alinanFirma"
        },
        {
            title:"Alınan Firma İrtibat",
            dataIndex:"alinanFirmaIrtibat",
            key:"alinanFirmaIrtibat"
        },

    ]
    const columns = [
        {
            title: 'Id',
            dataIndex: 'id',
            sorter: (a, b) => a.id - b.id,
            defaultSortOrder: 'ascend',
            sortOrder: 'ascend',
            key: 'id',
        },
        {

            title: 'Tip',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title:"Seri No",
            dataIndex:"seriNo",
            key:"seriNo"
        },
        {
            title:"Cihaz Markası",
            dataIndex:"marka",
            key:"marka"
        },
        {
            title:"Cihaz Modeli",
            dataIndex:"model",
            key:"model"
        },
        {
            title:"Lokasyon",
            dataIndex: "lokasyon",
            key: "lokasyon"
        },
        {
            title: "Mülkiyet",
            dataIndex: "mulkiyet",
            key: "mulkiyet"
        },
        {
            title: "Personel İsim",
            dataIndex: "personel",
            key: "personel",
            render: (text,record) => record.personel ? record.personel.isim : '',
        },
        {
            title: "Personel Soyad",
            dataIndex: "personel",
            key: "personel",
            render: (text, record) => record.personel ? record.personel.soyIsim : '',
        },
        {
          title: "İşlemler",
          render: (record)=>{
              return(
                  <>
                      <EditOutlined
                          title={"Demirbaşı Düzenle"}
                          style={{color:"#1777ff"}}
                          onClick={() => {
                              editDemirbas(record);
                          }}
                  ></EditOutlined>
                      <DeleteOutlined
                          title={"Demirbaşı Sil"}
                          style={{ marginLeft: 12,color:"#001528FF"  }}
                          onClick={()=>{deleteDemirbas(record);}}
                      ></DeleteOutlined>
                  </>
              )
          }
        },
        Table.EXPAND_COLUMN,
        {
            title: "Detay",
            dataIndex: "detay",
            key:"detay"
        },
    ]
    const options = [
        {
            value:"Bilgisayar",
            label:"Bilgisayar"
        },
        {
            value:"Monitör",
            label:"Monitör"
        },
        {
            value:"Çevre Birimleri",
            label: "Çevre Birimleri"
        },
        {
            value:"Diğer",
            label:"Diğer"
        }
    ]

    return (
        <div>
            <h1 style={{textAlign:"center",color:"black"}}>Demirbaş Listesi</h1>
            <Input.Search placeholder="Demirbaş Tipi Arama" allowClear onChange={e => setSearchText(e.target.value)} style={{marginBottom:16, width:"50%"}}></Input.Search>
            <Table pagination={{
                pageSize: 50,
            }}
                   scroll={{
                       y: 240,
                   }} columns={columns} dataSource={filterData(demirbas,searchText)} expandable={{expandedRowRender}} rowClassName={getRowClassName} size="small"></Table>
            <>
                <Button type="primary" style={{marginTop:"10px"}} onClick={showModal}>
                    Demirbaş Ekle
                </Button>
                <PDFDownloadLink document={<PDFDemirbasTable/>} fileName={"Envanter_Sistem"}>
                    {({loading}) =>(loading ? <Button style={{marginTop:"10px", color:"#E9F4FF", marginLeft:"10px", backgroundColor:"#001626"}}>Yükleniyor...</Button> : <Button style={{marginTop:"10px", color:"#E9F4FF", marginLeft:"10px", backgroundColor:"#001626"}}>
                        PDF İndir
                    </Button> )}

                </PDFDownloadLink>
                <Modal centered title="Demirbaş Ekle" style={{textAlign:"center"}} open={isModalOpen} okText={"Kaydet"} cancelText={"İptal"} onOk={handleOk} onCancel={handleCancel}>
                    <Select
                        style={{width:"100%"}}
                        value={addingDemirbas.type}
                        onChange={(value)=>{setAddingDemirbas((pre)=>{return {...pre,type:value};});setSelectedOption(value);}}
                        options={options} placeholder="Demirbaş Tipi">

                    </Select>
                    {selectedOption !== null && (
                        <>
                    <Input style={{marginTop:10}} value={addingDemirbas.seriNo} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,seriNo:e.target.value};});}} placeholder="Seri Numarası"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.lokasyon} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,lokasyon:e.target.value};});}} placeholder="Lokasyon"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.mulkiyet} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,mulkiyet:e.target.value};});}} placeholder="Mülkiyet"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.marka} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,marka:e.target.value};});}}  placeholder="Cihaz Markası"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.model} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,model:e.target.value};});}}  placeholder="Cihaz Modeli"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.cpu} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,cpu:e.target.value};});}}  placeholder="İşlemci"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.gpu} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,gpu:e.target.value};});}}  placeholder="Ekran Kartı"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.ram} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,ram:e.target.value};});}}  placeholder="Ram"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.hafiza} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,hafiza:e.target.value};});}}  placeholder="Depolama"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.ekranBoyutu} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,ekranBoyutu:e.target.value};});}}  placeholder="Ekran Boyutu"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.ekranCozunurlugu} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,ekranCozunurlugu:e.target.value};});}}  placeholder="Ekran Çözünürlüğü"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.tahsisTarihi} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,tahsisTarihi:e.target.value};});}}  placeholder="Tahsis Tarihi"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.alimTarihi} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,alimTarihi:e.target.value};});}}  placeholder="Alım Tarihi"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.alinanFirma} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,alinanFirma:e.target.value};});}}  placeholder="Alınan Firma"></Input>
                    <Input style={{marginTop:10}} value={addingDemirbas.alinanFirmaIrtibat} onChange={(e)=>{setAddingDemirbas((pre)=>{return {...pre,alinanFirmaIrtibat:e.target.value};});}}  placeholder="Alınan Firma İrtibat"></Input>
                    <Select style={{width:"100%", marginTop:10}}
                            defaultValue={"Personel Seçimi"}
                            onChange={(value) =>{setAddingDemirbas((pre)=>{return{...pre,personelId:value};});}}
                            placeholder="Personel seçimi">
                        {personel.map(item =>(
                            <Option key={item.id} value={item.id}>
                                {`${item.id} - ${item.isim} ${item.soyIsim}`}
                            </Option>))}
                    </Select>
                        </>
                    )}
                </Modal>

                <Modal
                    title="Demirbaş düzenle"
                    open={isEditing}
                    okText="Kaydet"
                    onCancel={() => {
                        resetEditing();
                    }}
                    onOk={() =>{
                        setExpandTable((pre)=>{
                            return pre.map((demirbas)=> {
                                if (demirbas.id === editingDemirbas.id) {
                                    return handleChangeOk();
                                } else {
                                    return demirbas;
                                }
                            });
                        });
                        resetEditing();
                    }}
                >
                    <Select style={{width:"100%"}}  options={options} value={editingDemirbas?.type}
                            onChange={(selectedOption) => {
                                setEditingDemirbas((prev) => {
                                    return { ...prev, type: selectedOption };
                                });
                            }}
                    ></Select>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.seriNo}
                        placeholder="Seri No"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, seriNo: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.lokasyon}
                        placeholder="Lokasyon"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, lokasyon: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.mulkiyet}
                        placeholder="Mülkiyet"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, mulkiyet: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.marka}
                        placeholder="Marka"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, marka: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.model}
                        placeholder="Model"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, model: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.cpu}
                        placeholder="İşlemci"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, cpu: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.gpu}
                        placeholder="Ekran Kartı"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, gpu: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.hafiza}
                        placeholder="Depolama"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, hafiza: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.ekranBoyutu}
                        placeholder="Ekran Boyutu"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, ekranBoyutu: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.ekranCozunurlugu}
                        placeholder="Ekran Çözünürlüğü"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, ekranCozunurlugu: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.tahsisTarihi}
                        placeholder="Tahsis Tarihi"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, tahsisTarihi: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.alimTarihi}
                        placeholder="Alım Tarihi"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, alimTarihi: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.alinanFirma}
                        placeholder="Alınan Firma"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, alinanFirma: e.target.value };
                            });
                        }}
                    ></Input>
                    <Input
                        style={{ marginTop: 10 }}
                        value={editingDemirbas?.alinanFirmaIrtibat}
                        placeholder="Alınan Firma İrtibat"
                        onChange={(e) => {
                            setEditingDemirbas((pre) => {
                                return { ...pre, alinanFirmaIrtibat: e.target.value };
                            });
                        }}
                    ></Input>
                    <Select style={{width:"100%", marginTop:10}} value={editingDemirbas?.personel?.id ?? 0} onChange={(selectedOption) =>{setEditingDemirbas((pre)=>{return{...pre,personel:personel.find(p=> p.id===selectedOption)};});}} placeholder="Personel Seçimi">
                        {personel.map(item =>(<Option  key={item.id} value={item.id}>{item.isim} {item.soyIsim}</Option>))}
                    </Select>
                </Modal>
            </>

        </div>
    )
}

export default EnvanterList