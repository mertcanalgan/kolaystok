import React, {useEffect, useState} from 'react'
import {Button, Form, Input,message} from "antd";
import logo from "../images/logo.png"
import axios from "axios";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import {useNavigate} from "react-router-dom";

function LogInPage() {
    const [formLog] = Form.useForm();
    const [admin,setAdmin] = useState([]);
    const [sifre,setSifre] = useState();
    const [kullaniciAdi,setKullaniciAdi] = useState();
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const navigate = useNavigate();
    message.config({
        duration: 1,
    });

    useEffect(()=>{
        axios.get("http://localhost:8080/api/admin")
            .then((res)=>setAdmin(res.data))
            .catch((err)=>console.log(err));
    },[]);
    useEffect(() => {
        const expirationTime = localStorage.getItem("expirationTime");
        const now = new Date().getTime();

        if (expirationTime && now > expirationTime) {
            // Clear localStorage
            localStorage.removeItem("kullaniciAdi");
            localStorage.removeItem("sifre");
            localStorage.removeItem("expirationTime");
        } else {
            // Set 1 minute timeout
            const expireTime = now + 60000; // 1 minute in milliseconds
            localStorage.setItem("expirationTime", expireTime);
            setTimeout(() => {
                localStorage.removeItem("kullaniciAdi");
                localStorage.removeItem("sifre");
                localStorage.removeItem("expirationTime");
            }, 60000*30);
        }
    }, []);


    const handleChangeKullaniciAdi = (event) =>{
        setKullaniciAdi(event.target.value);
    };
    const handeChangeSifre = (event) =>{
        setSifre(event.target.value);
    };
    const logIn = () => {
        const match = admin.find((item)=> kullaniciAdi === item["kullaniciAdi"] && sifre === item["sifre"])
        if (match) {
            localStorage.setItem("kullaniciAdi", kullaniciAdi);
            localStorage.setItem("sifre", sifre);
            setIsLoggedIn(true);
        } else {
            setIsLoggedIn(false);
            message.error("Giriş Başarısız");
        }
    }
    useEffect(() => {
        if (isLoggedIn) {
            message.success("Giriş Başarılı");
            navigate("/anasayfa");
        }
    }, [isLoggedIn]);
    return (
            <div style={{
                textAlign:"center",
                display:"flex",
                justifyContent:"center",
                alignItems:"center",
                height:"75vh"
            }}>

                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{
                        remember: true,
                    }}
                    form={formLog}
                >
                    <img alt="logo" src={logo}/>
                    <Form.Item
                        name="kullaniciAdi"
                        rules={[
                            {
                                required: true,
                                message: "Kullanıcı adı girilmeli!",
                            },
                        ]}
                    >
                        <Input
                            prefix={<UserOutlined className="site-form-item-icon" />}
                            placeholder="Kullanıcı Adı"
                            value="kullaniciAdi"
                            onChange={handleChangeKullaniciAdi}
                        />
                    </Form.Item>
                    <Form.Item
                        name="sifre"
                        rules={[
                            {
                                required: true,
                                message: "Şifre Girilmeli!",
                            },
                        ]}
                    >
                        <Input
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            value="sifre"
                            placeholder="Şifre"
                            onChange={handeChangeSifre}
                        />

                    </Form.Item>

                    <Form.Item>
                        <Button
                            style={{marginLeft:15}}
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                            onClick={logIn}
                        >
                            Giriş Yap
                        </Button>
                    </Form.Item>
                </Form>
            </div>
    )
}

export default LogInPage