import React, {useEffect, useState} from 'react';
import {Page,Text,Image,Document,StyleSheet} from "@react-pdf/renderer";
import logo from "../images/logo.png";
import axios from "axios";

const styles = StyleSheet.create({
    body:{
        paddingTop:35,
        paddingBottom:65,
        paddingHorizontal:35,
    },
    title:{
      fontSize:24,
      textAlign:"center",
    },
    text:{
        margin:12,
        fontSize:14,
        textAlign:"justify",
        fontFamily:"Times-Roman",
    },
    image: {
        marginVertical :15,
        marginHorizontal:100,
    },
    imza:{
        marginTop:12,
        fontSize:14,
        fontFamily:"Times-Roman",
        textAlign:"right",
        justifyContent:"flex-end"
    },
    header:{
        fontSize:12,
        marginBottom:20,
        textAlign:"center",
        color:"grey",
    },
    pageNumber:{
        position:"absolute",
        fontSize:12,
        bottom:30,
        left:0,
        right:0,
        textAlign:"center",
        color:"grey"
    }

})
const PDFFile = (props) => {
    const [personelZimmet, setPersonelZimmet] = useState([]);
    const [setPdfPersonel] = useState(null);
    useEffect(() => {
        const fetchData = async () => {
            try {
                const result = await axios.get(`http://localhost:8080/api/demirbas/${props.merhaba.id}`);
                setPersonelZimmet(result.data);
            } catch (err) {
            }
        };

        fetchData();
    }, []);

    useEffect(() => {
        if (personelZimmet.length > 0) {
            setPdfPersonel(personelZimmet[0]);
        }
    }, [personelZimmet]);

    return (
            <Document>
                <Page style={styles.body}>
                    <Text style={styles.header}></Text>
                    <Image style={styles.image} src={logo}></Image>
                    <Text style={styles.text}>{props.merhaba.isim} {props.merhaba.soyIsim}  isim ve soyisimli personelimize
                        sirketimiz tarafindan zimmetlenen demirbaslar asagidaki gibidir</Text>
                    {personelZimmet.map((item, index) => (
                        <Text key={index} style={styles.text}>
                            {index + 1}-) {item.mulkiyet} mülkiyetinde ve {item.lokasyon} konumunda bulunan {item.tahsisTarihi} tahsis tarihli {item.seriNo} seri numarali {item.marka} {item.model}
                        </Text>
                    ))}
                    <Text style={styles.imza}>Onayliyorum</Text>
                    <Text style={styles.pageNumber} render={({pageNumber,totalPages})=>`${pageNumber}/ ${totalPages}`} fixed></Text>
                </Page>
            </Document>
        )
};

export default PDFFile;
