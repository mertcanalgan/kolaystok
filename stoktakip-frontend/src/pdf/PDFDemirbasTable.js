import React, {useEffect, useState} from 'react';
import { Page, Text, View, Document } from '@react-pdf/renderer';
import axios from "axios";

const styles = {
    page: {
        backgroundColor: '#ffffff',
        padding: 20,
    },
    tableRow: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#bfbfbf',
        alignItems: 'center',
        height: 40,
    },
    tableCol: {
        width: '25%',
        borderLeftWidth: 1,
        borderLeftColor: '#bfbfbf',
        textAlign: 'center'
    },
    tableCell: {
        fontSize: 10,
        textAlign: "center",
        wordWrap: "break-word"
    },
};

const PDFDemirbasTable = () => {
    const [data,setData] = useState([]);
    useEffect(() => {
        const fetchData = async () => {
            try {
                const result = await axios.get(`http://localhost:8080/api/demirbas`);
                setData(result.data);
            } catch (err) {
                console.log(err);
            }
        };
        fetchData();
    }, []);


    return (
        <Document>
            <Page size="A1" style={styles.page}>
                <View>
                    <View style={styles.tableRow}>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Id</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Tip</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Lokasyon</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Mulkiyet</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Personel Isim</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Personel Soyad</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Seri No</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Cihaz Markasi</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Cihaz Modeli</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Islemci</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Ram</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Depolama</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Ekran Karti</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Ekran Boyutu</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Ekran Cozunurlugu</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Tahsis Tarihi</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Alim Tarihi</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Alinan Firma</Text>
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>Alinan Firma Irtibat</Text>
                        </View>
                    </View>
                    {data.map((item, index) => (
                        <View key={index} style={styles.tableRow}>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.id}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.type}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.lokasyon}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.mulkiyet}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.personel?.isim === undefined ? " " : item.personel.isim}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.personel?.soyIsim === undefined ? " " : item.personel.soyIsim}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.seriNo}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.marka}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.model}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.cpu}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.ram}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.hafiza}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.gpu}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.ekranBoyutu}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.ekranCozunurlugu}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.tahsisTarihi}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.alimTarihi}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.alinanFirma}</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{item.alinanFirmaIrtibat}</Text>
                            </View>
                        </View>
                    ))}
                </View>
            </Page>
        </Document> );
}



export default PDFDemirbasTable;